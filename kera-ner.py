#!/usr/bin/env python3
import tensorflow as tf
print("tensorflow version:", tf.__version__)
import numpy as np
import pandas as pd

vecs = pd.read_csv("../mockup/staging/word-vecs.csv.bz2", sep=',')
print(vecs.columns)

model = tf.keras.models.Sequential([
   tf.keras.layers.SimpleRNN(30, input_shape=(10,5,2)),
   tf.keras.layers.Dense(50, activation='relu'),
   tf.keras.layers.Dense(4, activation='softmax')
])

exit(0)


model.compile(optimizer=tf.keras.optimizers.SGD(learning_rate=0.1),
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

model.fit(xtrain, ytrain, epochs=300, batch_size=10, verbose=1)
print("fitting done")
p = model.evaluate(xtest, ytest, verbose=0)
print("final peformance:\n",
      np.stack((model.metrics_names, p), axis=1))

## Now greate the regular grid
ex1 = np.linspace(x[:,0].min(), x[:,0].max(), 100)
ex2 = np.linspace(x[:,1].min(), x[:,1].max(), 100)
xx1, xx2 = np.meshgrid(ex1, ex2)
                           # unlike R-s 'expand.grid', meshgrid creates matrices
g = np.stack((xx1.ravel(), xx2.ravel()), axis=1)
                           # we create the design matrix by stacking the xx1, xx2
                           # after unraveling those into columns
## predict on the grid
phat = model.predict(g)
hatY = np.apply_along_axis(np.argmax, 1, phat).reshape(100,100)
                           # imshow wants a matrix, so we reshape the predicted
                           # vector into one
plt.imshow(hatY, extent=(x[:,0].min(), x[:,0].max(), x[:,1].min(), x[:,1].max()),
           interpolation='none', origin='lower',
                           # you need to specify that the image begins from below,
                           # not above, otherwise it will be flipped around
           alpha=0.3)
plt.scatter(x[:,0], x[:,1], c=y, edgecolor='k')
plt.show()
