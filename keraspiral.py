#!/usr/bin/env python3
import tensorflow as tf
print("tensorflow version:", tf.__version__)
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

def createSpiral(n):
   x = np.random.normal(size=(n,2))
   r =  np.apply_along_axis(np.linalg.norm, 1, x)
   φ = np.arctan2(x[:,0], x[:,1])
   c1 = np.sin(2*φ + 2*r) > 0
   c2 = np.cos(2*φ + 2*r) > 0
   y = 2*c1 + c2
   return(x, y)

x, y = createSpiral(1000)
xtrain, xtest, ytrain, ytest = train_test_split(x, y, test_size=0.2)


model = tf.keras.models.Sequential([
   tf.keras.layers.Input(shape=(2,)),
   tf.keras.layers.Dense(50, activation='relu'),
   tf.keras.layers.Dense(4, activation='softmax')
])

model.compile(optimizer=tf.keras.optimizers.SGD(learning_rate=0.1),
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

model.fit(xtrain, ytrain, epochs=300, batch_size=10, verbose=1)
print("fitting done")
p = model.evaluate(xtest, ytest, verbose=0)
print("final peformance:\n",
      np.stack((model.metrics_names, p), axis=1))

## Now greate the regular grid
ex1 = np.linspace(x[:,0].min(), x[:,0].max(), 100)
ex2 = np.linspace(x[:,1].min(), x[:,1].max(), 100)
xx1, xx2 = np.meshgrid(ex1, ex2)
                           # unlike R-s 'expand.grid', meshgrid creates matrices
g = np.stack((xx1.ravel(), xx2.ravel()), axis=1)
                           # we create the design matrix by stacking the xx1, xx2
                           # after unraveling those into columns
## predict on the grid
phat = model.predict(g)
hatY = np.apply_along_axis(np.argmax, 1, phat).reshape(100,100)
                           # imshow wants a matrix, so we reshape the predicted
                           # vector into one
plt.imshow(hatY, extent=(x[:,0].min(), x[:,0].max(), x[:,1].min(), x[:,1].max()),
           interpolation='none', origin='lower',
                           # you need to specify that the image begins from below,
                           # not above, otherwise it will be flipped around
           alpha=0.3)
plt.scatter(x[:,0], x[:,1], c=y, edgecolor='k')
plt.show()
