#!/usr/bin/env julia

module mlp
export gradientascent, loglik
export nnet

using Plots
using Distributions
using LinearAlgebra
using MLDataUtils
using StatsFuns
using Calculus
using Random

## ---------------------- network function layers ----------------------
"""
function and gradient for a single nnet layer
"""
struct LayerFunc
    func::Function
    grad::Function
end

struct Layer
    func::Function
    grad::Function
    n::Integer
end

Layer(layerfunc, n) = begin
    Layer(layerfunc.func, layerfunc.grad, n)
end

## Logit layer
Logit = LayerFunc(x -> logistic.(x),
                  x -> exp.(x)./(1 .+ exp.(x)).^2
                  )

## MNLogit layer
mnlogitprob(x) = hcat(exp.(x), ones(size(x, 1))) ./ (sum(exp.(x), dims=2) .+ 1)
precompile(mnlogitprob, (Float64,))

"""
mn logit gradient
x: vector of values
y: which component.  nothing: all components as a matrix
"""
function mnlogitgrad(x::Array; y=nothing)
    p = mnlogitprob(x)
    if y === nothing
        grad = [-p[i]*p[j] for i in 1:size(p, 2) - 1, j in 1:size(p, 2)]
        grad[diagind(grad)] = grad[diagind(grad)] .+ p[1:size(p, 2)-1]
    else
        py = [ p[i,y[i]] for i in eachindex(y) ]
        grad = -p[:,1:end - 1] .* py
        for i in 1:size(grad, 1)
            if y[i] .< size(p, 2)
                grad[i, y[i]] += py[i]
            end
        end
    end
    grad
end
precompile(mnlogitgrad, (Array{Float64,1}, Array{Int,1}))
precompile(mnlogitgrad, (Array{Float64,2}, Array{Int,1}))
precompile(mnlogitgrad, (Array{Float64,2}, nothing))
mnlogit = LayerFunc(
    x -> mnlogitprob(x),
    (x; y=nothing) -> mnlogitgrad(x; y=y)
)

## RELU layer
Relu = LayerFunc(x -> max.(x, 0.0),
                 x -> convert(Array{Float64,2}, x .> 0.0)
                 )

## Softplus layer
softplus = LayerFunc(x -> log.(1 .+ exp.(x)),
                     x -> logistic.(x)
                     )

## -------------------- define and initialize layers --------------------
l¹ = nothing
lʸ = nothing
nˣ = nothing
nparam = nothing
pmask = nothing
iw = ib = iwy = ibʸ = nothing
# location of the parameters in the parameter vector
"""
initialize the network: set certain module variables
inputs:
n⁰: number of inputs
layer¹: Layer, the structure of the first hidden layer
layerʸ: Layer, structure of the output layer
module variables set:
pmask: has 1-s in place of weights, 0-s in place of biases
"""
function Initialize(n⁰::Int, layer¹::Layer, layerʸ::Layer)
    println("initializing network")
    global l¹, lʸ, nˣ
    l¹ = layer¹
    lʸ = layerʸ
    nˣ = n⁰
    global iw, ib, iwy, ibʸ
    iw = 1:(n⁰*l¹.n)
    ib = last(iw) .+ (1:l¹.n)
    iwy = last(ib) .+ (1:l¹.n*lʸ.n)
    ibʸ = last(iwy) .+ (1:lʸ.n)
    global nparam
    nparam = l¹.n*(n⁰ + 1) + lʸ.n*(l¹.n + 1)
    global pmask
    imask = [l¹.n*n⁰ .+ (1:l¹.n);  (nparam - lʸ.n + 1):nparam]
    pmask = ones(Int, nparam)
    pmask[imask] .= 0
end
precompile(Initialize, (Layer, Layer))

## ---------------------- main functions ----------------------

function agrad(θ::Array{Float64,1}, λ::Float64, x::Array{Float64,2}, y::Array{Int64,1})::Array{Float64,1}
    (W¹, b¹, Wʸ, bʸ) = splitparam(θ)
    c¹ = x*W¹ .+ transpose(b¹)
    z¹ = l¹.func.(c¹)
    cʸ = z¹*Wʸ .+ transpose(bʸ)
    phatmat = lʸ.func(cʸ)
    # matrix of all probabilities
    phat = [ phatmat[i, y[i]] for i in 1:size(phatmat, 1) ]
    # the probability corresponding to the actual evant
    #
    ∂log = 1 ./ phat
    #   ∂ phat/ ∂ cʸ
    ∂cʸ = ∂log .* (lʸ.grad(cʸ, y=y))
    # the last column is the dependent one, ignore that
    y_bʸ = ∂cʸ
    y_wʸ = [ kron(∂cʸ[i,:], z¹[i,:]) for i in 1:size(z¹, 1) ]
    # this gives array of arrays, how to do it in a simpler way?
    y_wʸ = permutedims(reshape(hcat(y_wʸ...), (length(y_wʸ[1]), length(y_wʸ))))
    # hack from https://stackoverflow.com/questions/24522638/julia-transforming-an-array-of-arrays-in-a-2-dimensional-array
    ##
    ∂z¹ = repeat(reshape(∂cʸ, size(∂cʸ, 1), 1, size(∂cʸ, 2)), 1, l¹.n, 1) .*
        repeat(reshape(Wʸ, 1, size(Wʸ, 1), size(Wʸ, 2)), size(x, 1), 1, 1)
    # gradient wrt z: tensor of n, n¹, nʸ
    ∂z¹ = reshape(sum(∂z¹, dims=3), size(∂z¹, 1), size(∂z¹, 2))
    ∂c¹ = ∂z¹ .* l¹.grad(c¹)
    y_b¹ = ∂c¹
    y_w¹ = [ kron(∂c¹[i,:], x[i,:]) for i in 1:size(x, 1) ]
    y_w¹ = permutedims(reshape(hcat(y_w¹...), (length(y_w¹[1]), length(y_w¹))))
    #
    grad = hcat(y_w¹, y_b¹, y_wʸ, y_bʸ)
    grad = sum(grad, dims=1) ./ length(y)
    #
    g = vec(rotr90(grad) .- 2λ.*(θ .* pmask))
    g
end
precompile(agrad, (Array{Float64,1}, Float64, Array{Float64,2}, Array{Float64,1}))

function comparederivatives(θ, λ, x, y)
    num = numgrad(θ, λ, x, y)
    anal = agrad(θ, λ, x, y)
    println("param\tnum\tanal\tnum-anal\trelative")
    display(hcat(θ, num, anal, num-anal, (num-anal)./anal))
end

"""
    Display the contour plot of the loglik function and the corresponding gradient field
    inputs:
    θ: the parameter value to use
    λ: penalty parameter
    x, y: data
    i, j: which components to plot
    wᵢ, wⱼ: width of the plot window for i, j
    α: length of gradient arrows.  α=0 – no arrows
    returns:
    plot p
    """
function contourgrad(θ, λ, x, y; i=2, j=3, wᵢ=0.5, wⱼ=0.5, ngrid=25)
    rxᵢ = LinRange(θ[i] - wᵢ/2, θ[i] + wᵢ/2, ngrid)
    rxⱼ = LinRange(θ[j] - wⱼ/2, θ[j] + wⱼ/2, ngrid)
    loglikmat = [ loglikwrap([xᵢ xⱼ], θ, λ, x, y; i=i, j=j) for xⱼ in rxⱼ, xᵢ in rxᵢ]
    # remember: column-major order; rows are on the vertical axis!
    gr()
    contPlot = Plots.contour(rxᵢ, rxⱼ, loglikmat,
                             xlabel="component "*string(i),
                             ylabel="component "*string(j))
    ex₀ = θ[i]
    ey₀ = θ[j]
    scatter!(contPlot, [ex₀], [ey₀], color=:goldenrod3,
             msw=0.5, markersize=1.5, legend=false)
    ## determine gradient scale
    gradmat = [ gradwrap([xᵢ xⱼ], θ, λ, x, y; i=i, j=j) for xⱼ in rxⱼ, xᵢ in rxᵢ]
    maxNorm = maximum([norm(g) for g in gradmat])
    w =  norm([wᵢ wⱼ])
    α = 0.03/maxNorm*w
    # make the longest arrow 2% of the image diagonal
    gxᵢ = α*vec([gradmat[k,l][i] for k in 1:size(gradmat, 1), l in 1:size(gradmat, 2)])
    gxⱼ = α*vec([gradmat[k,l][j] for k in 1:size(gradmat, 1), l in 1:size(gradmat, 2)])
    vrxᵢ, vrxⱼ = repeat(rxᵢ, inner=length(rxⱼ)), repeat(rxⱼ, outer=length(rxᵢ))
    Plots.quiver!(contPlot, vrxᵢ, vrxⱼ, gradient=(gxᵢ, gxⱼ),
                  arrow=arrow(0.01, 0.01), alpha=0.5, linewidth=0.5)
    contPlot
end

function numgrad(θ, λ, x, y)
    Calculus.gradient(ϑ -> loglik(ϑ, λ, x, y), θ)
end    

"""
computes the gradient at θ₀ where components i,j are swapped out with θ₂
inputs:
  θ₂: 2d vector, values to replace the original
  θ₀: parameter vector, components i,j will be replaced by θ₂
  λ: penalty parameter
returns:
  the gradient vector
"""
function gradwrap(θ₂, θ₀, λ, x, y; i=2, j=3)
    θ = copy(θ₀)
    θ[[i j]] = θ₂
    agrad(θ, λ, x, y)
end

"""
compute log-likelihood, including regularization penalty
uses 'nnet' to predict probability
"""
function loglik(θ, λ, x, y)
    phat = nnet(θ, x);
    # NxK matrix of outcome probabilities
    loglik = [ log(phat[i, y[i]]) for i in 1:size(phat, 1) ]
    # probability for the correct outcome
    ll = sum(loglik)/length(y) .- λ*θ' * (θ .* pmask)
    ll
end;

function loglikwrap(θ₂, θ₀, λ, x, y; i = 2, j = 3)
    θ = copy(θ₀)
    θ[[i j]] = θ₂
    loglik(θ, λ, x, y)
end

"""
    compute MLP output
    inputs:
    x : design matrix
    θ : parameter vector
    """
function nnet(θ::AbstractArray, x::AbstractArray)
    (W¹, b¹, Wʸ, bʸ) = splitparam(θ)
    c¹ = x*W¹ .+ transpose(b¹)
    z¹ = l¹.func.(c¹)
    cʸ = z¹*Wʸ .+ transpose(bʸ)
    phatmat = lʸ.func(cʸ)
    # predicted probability for each class
    phatmat
end;

function splitparam(θ)
    W¹ = reshape(θ[iw], (nˣ, l¹.n))
    b¹ = reshape(θ[ib], (l¹.n, 1))
    Wʸ = reshape(θ[iwy], (l¹.n, lʸ.n))
    bʸ = reshape(θ[ibʸ], (lʸ.n, 1))
    (W¹, b¹, Wʸ, bʸ)
end

### ---------- large end-user functions ----------

"""
    optimizes the function f with gradient ascent
      arguments:
    λ:  penalty
    ρ₀: initial learning rate.
    i, j, plot: which two components to plot if 'plot'==true
    """
function gradientascent(f, grad, θ₀, λ, X, y;
                        ρ₀=0.5, ρₜ=0.005, τ=1_000_000,
                        batchsize=nothing,
                        niter=300, nouter=4, patience=10,
                        ix=2, iy=3, wx=1, wy=1, ngrid=18, contours=true,
                        verbose=0, verbosestep=10_000)
    println("go here")
    Xshuffled, yshuffled = shuffleobs((X, y), obsdim=1)
    (Xtrain, ytrain), (Xvalid, yvalid) = splitobs((Xshuffled, yshuffled), at = 0.8, obsdim=1)
    if contours
        contPlot = contourgrad(θ₀, λ, X, y; i=ix, j=iy, wᵢ=wx, wⱼ=wy, ngrid=ngrid)
    end
    ρ = ρ₀
    llSize = nouter*(niter ÷ verbosestep)
    println("create storage for ", llSize, " iterations")
    llStorage = Array{Float64, 2}(undef, (llSize, 2))
    llIter = Vector{Int32}(undef, llSize)
    llStep = 0
    ## early stopping
    bestθ = θ₀
    bestLL = f(θ₀, λ, Xvalid, yvalid)
    iWorse = 0
    ##
    iteration = niter
    iterations = 1
    epoch = 1
    iBatch = 0
    # where the last batch ended
    batchSize = isnothing(batchsize) ? size(ytrain, 1) : batchsize
    for outer in 1:nouter
        for iter in 1:niter
            iBatch = last(iBatch) .+ collect(1:batchSize)
            if any(iBatch .> size(y,1))
                replace!(x -> x > size(y, 1) ? x - size(y, 1) : x, iBatch)
                epoch += 1
                # ρ = ρ₀/(1 + epoch/batchsize/4)
            end
            # modulo makes it to restart from beginning.
            gr = grad(θ₀, λ, X[iBatch,:], y[iBatch])
            θ = θ₀ + ρ * gr
            if contours
                Plots.scatter!(contPlot, [θ[ix]], [θ[iy]],
                               color=:red, alpha=0.4, markersize=1, msw=0,
                               legend=false)
            end
            ## printing
            if iter % verbosestep == 0
                ## predict on validation set
                llStep += 1
                ll = f(θ, λ, Xvalid, yvalid)
                llStorage[llStep, 1] = ll
                llStorage[llStep, 2] = norm(gr)
                llIter[llStep] = iter
                ρ = max(ρ₀ - iter/τ*(ρ₀ - ρₜ), ρₜ)
                # linear decay of learning rate, see GBC p287
                if ll > bestLL
                    bestθ = θ
                    bestLL = ll
                    iWorse = 0
                else
                    if iWorse >= patience
                        iteration = iter
                        break
                    end
                    iWorse += 1
                end
                if verbose > 0
                    println("epoch ", epoch, ", iteration ", outer, "/", iter,
                            ": test ll =", ll, ", ρ=", ρ)
                end
            end
            θ₀ = θ
            iterations = iter
        end
        iWorse = 0
        ρ \= 5
    end
    println("all $iterations done")
    (bestθ, bestLL, iteration, llStorage[1:llStep,:])
end

end
