#!/usr/bin/env julia
push!(LOAD_PATH, ".")
using Random
using LinearAlgebra
using Distributions
using Plots
#
using mlp

## -------------------- data creation --------------------

function createQuadrants(n)
    x1, x2 = randn(n), randn(n)
    x = hcat(x1, x2);
    y = trunc.(Int, 1 .+ (1 .+ sign.(x1 .* x2))./2)
    x, y
end

function createSpikes(n)
    x1, x2 = randn(n), randn(n)
    x = hcat(x1, x2);
    y = trunc.(Int, 2 .* (x1 .> 0) + (x2 .> 0)) .+ 1
    x, y
end

function createSpiral(n)
    x1, x2 = randn(n), randn(n)
    x = hcat(x1, x2);
    r =  sqrt.(x1.^2 .+ x2 .^ 2)
    φ = atan.(x1, x2)
    c¹ = sin.(2φ + 2r) .> 0
    c² = cos.(2φ + 2r) .> 0
    y = 2c¹ + c² .+ 1
    x, y
end    

function createXor(n)
    d = Binomial(1, 0.5)
    x1 = Float64.(rand(d, n))
    x2 = Float64.(rand(d, n))
    x = hcat(x1, x2);
    y = trunc.(Int, 1 .+ (1 .+ sign.(x1 .* x2))./2)
    x, y
end

β₀ = nothing
function startParam()
    β₀ = randn((mlp.nparam,))*0.1 .+ 0.5
    β₀[.!Bool.(mlp.pmask)] .= 0.1
    β₀
end

mlp.Initialize(2, mlp.Layer(mlp.Relu, 3), mlp.Layer(mlp.mnlogit, 1))
β₀ = startParam()

## -------------------- get ready for start --------------------
n = 500;
createQuadrants(n)

function invokeSGA(niter=1000; nouter=4, patience=10,
                   batch=nothing,
                   ρ₀=0.2, ρₜ=ρ₀/100, τ=niter,
                   λ=0.0,
                   contours=false, ix=1, iy=2, w=4,
                   nGrid=50,
                   vs=100, verbose=0)
    ## sanity checks
    @assert maximum(y) - 1 == mlp.lʸ.n
    ##
    println("  n iterations: ", niter)
    if batch === nothing
        println("  full gradient ascent")
    else
        println("  stochastic gradient ascent, batch size: ", batch)
    end
    println("  initial learning rate: ", ρ₀)
    println("  penalty λ: ", λ)
    println("  contours: ", contours)
    println("  plot ", ix, " on x, ", iy, " on y")
    println("  plot range: ", w)
    println("  verbosity printing step: ", vs)
    println("  size of x:", size(x))
    i = 1:size(x, 1)
    ##
    ##
    if size(β₀, 1) != mlp.nparam
        println("wrong dimension of parameters")
        return
    end
    println("initial parameter β₀:", size(β₀))
    # display(hcat(x[i,:], y[i])); println(" = x, y")
    # mlp.comparederivatives(β₀, λ, x[i,:], y[i])
    # return(0)
    ##
    data = Array{Float64,2}(undef, 0, 2)
    val = nothing
    β = β₀
    β, val, niter, data =
        gradientascent(mlp.loglik, mlp.agrad,
                       β, λ, x[i,:], y[i];
                       ρ₀=ρ₀, ρₜ=ρₜ, τ=τ,
                       batchsize=batch,
                       niter=niter, nouter=nouter, patience=patience,
                       ix=ix, iy=iy, wx=w, wy=w, contours=contours,
                       verbose=verbose, verbosestep=vs)
    println(niter, " iterations")
    println("\n  ", niter, " iterations")
    println("  function value: ", val)
    println("  norm of parameter: θ'θ: ", β'β)
    ##
    hatp = nnet(β, x);
    haty = findmax(hatp, dims=2)[2]
    haty = [haty[i,:][1][2] for i in 1:size(haty,1)]
    println("Sample results:\n",
            "x₁\tx₂\ty\that p\that y")
    display(hcat(x[i,:], y[i], hatp[i,:], haty[i])[1:min(20,last(i)),:])
    println("Accuracy:", mean(y .== haty))
    gr()
    Plots.scalefontsizes(0.4)
    convPlot = plot(data, layout=(2,1),
                    label = ["log-likelihood", "norm of grad"],
                    legendfontsize=10)
    ## ensure we don't plot too many points
    if(size(x, 1) > 2000)
        jd = sample(1:size(x, 1), 2000, replace=false)
        # plot only these dots
    else
        jd = 1:size(x,1)
    end
    if(size(x, 2) == 2)
        # 2d case, show decision boundary
        x¹ = range(minimum(x[:,1]), maximum(x[:,1]), length=nGrid)
        x² = range(minimum(x[:,2]), maximum(x[:,2]), length=nGrid)
        grid = hcat(repeat(x¹, inner=length(x²)), repeat(x², outer=length(x¹)))
        pGrid = nnet(β, grid)
        yGrid = findmax(pGrid, dims=2)[2]
        yGrid = [yGrid[i,:][1][2] for i in 1:size(yGrid,1)]
        ap = scatter(grid[:,1], grid[:,2], c=yGrid, alpha=0.5, ms=2,
                     markershape=:square, msw=0)
        scatter!(ap, x[jd,1], x[jd,2], c=y[jd],
                 markershape=:circle, ms=1, msw=0.2,
                 legend=false)
    else
        ap = scatter(x[jd,1], x[jd,2], c=haty[jd], ms=1.3)
    end
    l = @layout [ a{0.6w} b ]
    pl = plot(ap, convPlot, layout=l)
    display(pl)
end
