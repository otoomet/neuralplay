
##
import numpy as np
import tensorflow as tf
from tensorflow import keras
from tensorflow.keras import layers
import matplotlib.pyplot as plt

##

model = keras.Sequential()
model.add(layers.SimpleRNN(10, input_shape=(1,1)))
model.add(layers.Dense(1))
model.summary()

model.compile(
    loss=keras.losses.MeanSquaredError(),
    optimizer="adam"
)

##
n = 1000
x = np.arange(n).reshape((-1,1,1))
y = np.sin(0.1*x) + np.random.normal(scale=0.1)
y = y.ravel()

print("intput shape", x.shape, y.shape)
model.fit(
    x, y.ravel(),
    validation_split=0.2,
    epochs=100
)

##
xnew = np.arange(0, 120).reshape(-1,1,1)
yhat = model.predict(xnew)

plt.scatter(xnew.ravel(), yhat)
plt.show()
