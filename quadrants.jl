#!/usr/bin/env julia
module quadrants

push!(LOAD_PATH, ".")
using ArgParse
using Random
using RCall
using LinearAlgebra
#
using mlp

mlp.Initialize(Layer(mlp.Relu, 2), Layer(mnlogit, 1))
println(mlp.nparam, " parameters")

s = ArgParseSettings()
@add_arg_table s begin
    "--batch", "-b"
    help = "batch size"
    arg_type = Int
    default = nothing
    "--niter", "-i"
    help = "max number of iterations"
    arg_type = Int
    default = 1000
    "--ix"
    help = "component to be plotted on x axis"
    arg_type = Int
    default = 1
    "--iy"
    help = "component to be plotted on y axis"
    arg_type = Int
    default = 2
    "--lambda", "-l"
    help = "Loglik function l2 penalty"
    arg_type = Float64
    default = 0.01
    "--rho", "-r"
    help = "Learning rate"
    arg_type = Float64
    default = 0.1
    "--contours"
    help = "plot the contour map and the path?"
    action = :store_true
    "--seed", "-s"
    help = "random seed"
    arg_type = Int
    default = 0
    "--verbose", "-v"
    help = "verbosity level"
    arg_type = Int
    default = 0
    "--vs"
    help = "verbosity printing step"
    arg_type = Int
    default = 10_000
    "--wx"
    help = "plot x range"
    arg_type = Float64
    default = 0.5
    "--wy"
    help = "plot y range"
    arg_type = Float64
    default = 0.5
end

args = parse_args(ARGS, s)
println("  n iterations: ", args["niter"])
if args["batch"] === nothing
    println("  full gradient ascent (--batch)")
else
    println("  stochastic gradient ascent, batch size (--batch): ", args["batch"])
    println("  batch size: ", args["batch"])
end
println("  learning rate (--rho): ", args["rho"])
λ = args["lambda"]
println("  penalty λ (--lambda): ", λ)
seed = args["seed"]
println("  random seed (--seed): ", seed)
contours = args["contours"]
println("  contours (--contours): ", contours)
println("  plot ", args["ix"], " on x, ", args["iy"], " on y")
println("  plot x range (--wx): ", args["wx"], ", y range (--wy): ", args["wy"])
println("  verbosity printing step (--vs): ", args["vs"])

println("create data")
Random.seed!(seed)
n = 200;
x1 = randn(n);
x2 = randn(n);
x1 = vec([-1.0 -1.0 1.0 1.0])
x2 = vec([-1.0 1.0 -1.0 1.0])
x = hcat(x1, x2);
y = trunc.(Int, 1 .+ (1 .+ sign.(x1 .* x2))./2)
# R"plot($x1, $x2, col=$y, xlab='x1', ylab='x2')"
# R"abline(h=0, lty=2)"
# R"abline(v=0, lty=2)"
println("create network")
nˣ = 2;

println("start work: dim of X is ", size(x));
# which components are weights, not biases
β₀ = randn((mlp.nparam,))*0.1 .+ 0.5;
β₀[.!Bool.(mlp.pmask)] = repeat([0.1], 3)
β₀ = vec([5.94646 1.49724 6.58594 -1.55745 -2.48869 -3.29787 -2.73742 3.42976 -0.00104452])
# stuck here
β₀ = vec([-1.22 -1.22 1.22 1.22 -0.273 -0.273 -10.04 -10.04 8.81])
# xor perceptron solution where x ∈ {-1, 1}
# y(-1, -1) = y(1, 1) = 2
# y(-1, 1) = y(1, -1) = 1
β₀ = vec([-0.1 -0.1 -0.2 -0.2 0.273 0.273 -10.04 -10.04 8.81])
 
println("initial parameter β₀:")
display(β₀); println()
##
i = 1:4
# display(hcat(x[i,:], y[i])); println(" = x, y")
# mlp.comparederivatives(β₀, λ, x[i,:], y[i])
# exit(0)

β₁, val, grad = gradientascent(loglik, agrad, β₀, λ, x[i,:], y[i];
                    ρ₀=args["rho"],
                    batchsize=args["batch"],
                    niter=args["niter"],
                    ix=args["ix"], iy=args["iy"], wx=args["wx"], wy=args["wy"], contours=contours,
                    verbose=args["verbose"], verbosestep=args["vs"])
println("final parameters:\nvalue\tgradient:")
display(hcat(1:length(β₁), β₁, grad))
println("\n  function value: ", val)
println("  norm of gradient: ", LinearAlgebra.norm(grad, 1))
println("  norm of parameter: θ'θ: ", β₁'β₁)

hatp = nnet(β₁, x[i,:]);
haty = 1 .+ round.(Int, hatp[:,2] .> 0.5)
println("Sample results:\n",
        "x₁\tx₂\ty\that p\that y")
display(hcat(x[i,:], y[i], hatp, haty)[1:min(20,last(i)),:])
# R"points($x1, $x2, col=$haty, pch=2, cex=0.6)"
println()
a = readline()

end
