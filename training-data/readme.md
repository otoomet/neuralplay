# Code to create/work with training data

This folder mainly consists of Ajinkya's research explorations on NLP
component of Eviction Notices Project.

Following Modules are included:

1) Synthetic Data Generation  
    - Generates data from random eviction notices and some random legal text  
    - Utilities for text augmentation  
    - Creates a labelled dataset of notices and addresses  
  
2) Address Classification  
    - Works with 99% accuracy  
    - Inbuilt utilities to generate data on the fly  
  
3) Seq2Seq Model  
    - Failed Attempt  
    - Inappropriate to extract addresses from notices  
  
4) BIO Model  
    - (WIP)  

## Individual code files:


### address-classifier-data-v1.0

- Creates labelled synthetic data of addresses and non-addresses
- Complete control over creation of synthetic data
- Text Processing
- Text Classifier Model
	- RNNs, Attention, 1D-Conv

Input Files:
	.\synthetic-data\extract.txt
	

## other code
	
2. Embeddings-Alice.ipynb
- Visualize Embeddings in 3D of Alice in Wonderland text
- Auto Encoder Model
	- MLP
Input Files:
	.\alice-in-wonderland.txt
3. Embeddings-LSTM-Alice.ipynb
- Visualize Embeddings in 3D of Alice in Wonderland text
- Auto Encoder Model
	- LSTM
Input Files:
	.\alice-in-wonderland.txt
4. Embeddings.ipynb
- Visualize Embeddings of Nursery Rhymes Text
- Auto Encoder Model
	- MLP
Input Files:
	.\nursery-rhymes-dataset.txt
5. research.ipynb
- Labelled Synthetic Data Generation of addresses and eviction notices
- Text Augmentation Techniques
- Inappropriately Designed LSTM Seq2Seq Model (Scrap that)
Input Files:
	.\synthetic-data\*
	.\synthetic-data\extract.txt
	.\synthetic-data\text\*
Output Files:
	.\labeled_data.txt
5.b research-BKP.ipynb (An older checkpoint of research.ipynb)
6. seq2seq.ipynb\seq2seq_model.ipynb
- Appropriately Designed seq2seq Model (Enhancement over research.ipynb seq2seq model)
- Concluded that seq2seq model is not suitable for this usecase
Input Files:
	labeled_data.txt
7. modelling.ipynb
- Creates and Saves various seq2seq models based on research.ipynb
- All the models are design/use-case failures
- Can be exported as .py files for running a batch job on the server
Input Files:
	labeled_data.txt

