# neuralplay

Play with neural networks.

Trying to implement various neural networks and related model
fragments

## Topics

* Ch 10: in particular LSTM
* Not sure about Part III:
    * Ch 14: autoencoders?
    * Ch 15: representation learning?
	* Ch 20: deep generative models?


## code

* _mlp.jl_: multi-layer-perceptron, julia module
* _invoke.jl_: julia code to create data and call the _mlp_ module,
  intended to be run inside REPL
* _kera-ner.py_: NER using keras and RNN-s
* _keraspiral.py_: categorizes dots by color in a spiral fashion in
  keras/python
* _quadrants.jl_: julia code to create data and call _mlp_, to be run
  on command line
* _tfcbow.py_: creates CBOW embeddings using plain tensorflow, taken from the Ganegedara's
  book


## For Mar 27th

* Jose:
    * feeding data to RNN
* Ott: 
    * fight embeddings  
	if get it done, run on documents
	* Golberg RNN chapter
* Ajinkya
    * play with the model and try to figure out why did it fail
	* stress extraction over prediction
	* can you use GRU instead of LSTM?
* Ishita:
    * implement an RNN to show

## For Mar 20th

* Ishita: 
    * plug the data into the model, Ajinkya will update the data
* Ott:
    * continue fighting embeddings
	* read Golberg RNN chapter
	* read Jurafsky 3rd ed Ch 6 about embeddings
* Ajinkya:
    * play with the model and try to figure out why did it fail
	* stress extraction over prediction
	* can you use GRU instead of LSTM?

## For Mar 6th

* Ott:
    * understand the speed-up
	* read Golberg
	
* Ishita:
    * work with keras with the aim of address NER
	
* Jose:
    * sythesize, summarize Jurafsky ch 9

* Ajinkya:
    * fix data, implement character-based-sequence-model (in keras)
	* read Goodfellow, Jurafsky


## For Feb 28th

* Jose:
    * read Jurafsky & Martin about perplexity
	* think about embeddings
	* how to feed ngram data into bidirectional RNN
	
* Ajinkya:
    * understand LSTM-s
	* create training data/code

* Ott
    * speed up word2vec (if I can)
	* read Goldberg Recurrent Nets


## For Feb 21st

* Ott
    * speed up word2vec (if I can)
	* read goldberg Recurrent Nets
* Ajinkya 
    * LSTMs (read and understand, if possible implement)
* Ishita
    * create embeddings useing semi-general data (legal docs).  Check
      kaggle, 
	* play w/autoencoders, MLP-s
	

## For Jan 31th (9am)

* Ott:
    * check counts in log-likelihood
	* speed it up?
	* read Golberg (?)

* Jose
    * DS for SG proposal
	
	

## For Jan 24th

* Ott: 
    * order the book
	* look at embeddings code
    * read something (Goldberg?)
* Ishita:
    * read about embeddings
* Jose
    * read something 
* Ajinkya
    * Jurafsky Ch 6 (vectors and embeddings)


## For Dec 3rd

* Understand:
    * how do they calculate ngram probabilities for word embeddings?
      There seems to be curse of dimensionality already with
      bigrams...
	* how do they optimize word embeddings: do you have to optimize
      both embeddings matrix and weigths at the same time?
	  
* Read Ch 11


## For nov 19th

* Everyone:
    * matrix calculus: $\partial v/\partial v$ where $v = (v_1, v_2)$
	* read the archive paper
* Ishita:
    * read more of ch 10
	* learn keras
* Ott:
    * attempt to do a bidirectional network in keras to find
      minimalistic addresses
* Jose:
    * think about embeddings

## For Nov 12th

* Start using keras/tensorflow
* Read Ch 10: recurrent networks

* Ott: do the color spiral in keras.
    - add convolutions to be able to shift the spiral?
* Jose: play w/how to train embeddings


## In the future

* train our own word embeddings based on casefiles
    - compare with some sort of pre-trained ones/other corpora
* semi-supervised learning


# past stuff

## For Oct 29th:

* Ch 9: convolutional nets

* Ott: add layers, nodes, see if I can get quadrants to work
    - did not add nodes
	- quandrants work, color spiral (4 class) works too
	- issues with convergence, stops too early
* Jose: add more classes?  Maybe...
* Ishita: start implementing...

## earlier

* Ch 6: Feed forward nets
Develop a NN yourself, use gradient descent for optimization
* Ch 7, 8: Regularization, Optimization
Add some tricks from these chapters to our model
* Ch 9: Convolutional nets  
Add a convolutional layer?
