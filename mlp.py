import numpy 

def sigmoid (x):
    return 1/(1 + numpy.exp(-x))

def sigmoid_derivative(y):
    return y * (1 - y)

### Using rescaled logistic function 
####################################
# def tanh(x):
#     return (1.0 - numpy.exp(-2*x))/(1.0 + numpy.exp(-2*x))

# def tanh_derivative(x):
#     return (1 + x)*(1 - x)


# Create a class with procedures to build NN from scratch 
# Will need a list with layers of the network 
# XOR is a 2 x 2 x 1 --> [2,2,1]

class NeuralNetwork:
    """
    Parameters
    ----------
    self: 'NeuralNetwork' class object
    net_arch: list of integers, how many neurons per layer
    """
    # Initialize
    def __init__(self, net_arch):
        numpy.random.seed(0)
        # Initialized the weights, making sure we also 
        # initialize the weights for the biases that we will add later
        self.activity = sigmoid
        self.activity_derivative = sigmoid_derivative
        self.layers = len(net_arch)
        self.steps_per_epoch = 1
        self.arch = net_arch
        self.weights = []
        # Random initialization with range of weight values (-1,1)
        for layer in range(self.layers - 1):
            w = 2*numpy.random.rand(net_arch[layer] + 1, net_arch[layer+1]) - 1
            self.weights.append(w)
    # 
    def _forward_prop(self, x):
        y = x
        
        for i in range(len(self.weights)-1):
            activation = numpy.dot(y[i], self.weights[i])
            activity = self.activity(activation)
            # add bias for the next layer
            activity = numpy.concatenate((numpy.ones(1), numpy.array(activity)))
            y.append(activity)
        # last layer
        activation = numpy.dot(y[-1], self.weights[-1])
        activity = self.activity(activation)
        y.append(activity)
        
        return y
    # Back porpagation
    def _back_prop(self, y, target, learning_rate):
        error = target - y[-1]
        delta_vec = [error * self.activity_derivative(y[-1])]
        # begin from the back, next to last layer
        for i in range(self.layers-2, 0, -1):
            error = delta_vec[-1].dot(self.weights[i][1:].T)
            error = error*self.activity_derivative(y[i][1:])
            delta_vec.append(error)
        # Set values from back to front 
        delta_vec.reverse()
        # Adjust the weights, using backprop rules
        for i in range(len(self.weights)):
            layer = y[i].reshape(1, self.arch[i]+1)
            delta = delta_vec[i].reshape(1, self.arch[i+1])
            self.weights[i] += learning_rate*layer.T.dot(delta)
    # Fitting function
    def fit(self, data, labels, learning_rate=0.1, epochs=100):
        """
        parameters
        ----------
        data: the set of all possible pairs of booleans True or False indicated by the integers 1 or 0
        labels: the result of the logical operation 'xor' on each of those input pairs
        """
        # Add 1 to the input data (always "on" neuron)
        ones = numpy.ones((1, data.shape[0]))
        Z = numpy.concatenate((ones.T, data), axis=1)
        
        for k in range(epochs):
            if (k+1) % 10000 == 0:
                print('epochs: {}'.format(k+1))
                
            sample = numpy.random.randint(X.shape[0])
            # Feed forward propagation
            x = [Z[sample]]
            y = self._forward_prop(x)
            # Back propagation
            target = labels[sample]
            self._back_prop(y, target, learning_rate)
    # Predict
    def predict_single_data(self, x):
        """
        parameters
        ----------
        x: single input data
        """
        val = numpy.concatenate((numpy.ones(1).T, numpy.array(x)))
        for i in range(0, len(self.weights)):
            val = self.activity(numpy.dot(val, self.weights[i]))
            val = numpy.concatenate((numpy.ones(1).T, numpy.array(val)))
        return val[1]
    # Predictions 
    def predict(self, X):
        """
        parameters
        ----------
        X: the input data array
        """
        Y = numpy.array([]).reshape(0, self.arch[-1])
        for x in X:
            y = numpy.array([[self.predict_single_data(x)]])
            Y = numpy.vstack((Y,y))
        return Y


#######
# TEST IT OUT
#######
# 2 input neurons
# 2 hidden neurons
# 1 output neuron
nn = NeuralNetwork([2,2,1])

# Input data
X = numpy.array([[0, 0], [0, 1],
                [1, 0], [1, 1]])

# Labels, the correct results
y = numpy.array([0, 1, 
                 1, 0])

# Call the fit function and train the network for a chosen number of epochs
nn.fit(X, y, epochs=100000)

# Show the prediction results
for s in X:
    print(s, nn.predict_single_data(s)) 
