#!/usr/bin/env python3
# coding: utf-8
import tensorflow as tf
print("tensorflow version:", tf.__version__)
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split

def createSpiral(n):
   x = np.random.normal(size=(n,2))
   r =  np.apply_along_axis(np.linalg.norm, 1, x)
   φ = np.arctan2(x[:,0], x[:,1])
   c1 = np.sin(2*φ + 2*r) > 0
   c2 = np.cos(2*φ + 2*r) > 0
   y = 2*c1 + c2
   x = x.astype('float32')
   return(x, y)

def encode1hot(y):
    y1hot = np.zeros((len(y), len(np.unique(y))), dtype=np.float32)
    y1hot[range(len(y)), y] = 1.0
    return y1hot
x, y = createSpiral(2000)
xtrain, xtest, ytrain, ytest = train_test_split(x, y, test_size=0.2)
## Create one-hot encoded labels: needed for 'tf.nn.softmax_cross_entropy_with_logits_v2'
labels1hot = encode1hot(ytrain)
testlabels1hot = encode1hot(ytest)

## Defining Hyperparameters and Some Constants
WEIGHTS_STRING = 'weights'
BIAS_STRING = 'bias'
batch_size = 100
inputSize = 2
layer1Size = 20
layer2Size = 20
numLabels = len(np.unique(y))
NUM_EPOCHS = 1530
ρ = 0.02

# resets the default graph Otherwise raises an error about already initialized variables
tf.reset_default_graph()

## Defining Input and Label Placeholders
tf_inputs = tf.placeholder(shape=[batch_size, inputSize], dtype=tf.float32, name = 'inputs')
tf_labels = tf.placeholder(shape=[batch_size, numLabels], dtype=tf.float32, name = 'labels')
# note: must be of the same data type as likelihood

## Defining the Weights and Bias Variables (with Scoping)
# Defining the Tensorflow variables
def define_net_parameters():
    with tf.variable_scope('hidden1'):
        tf.get_variable(WEIGHTS_STRING, shape=[inputSize, layer1Size],
                        initializer=tf.random_normal_initializer(0, 0.02))
        tf.get_variable(BIAS_STRING, shape=[layer1Size],
                        initializer=tf.random_uniform_initializer(0, 0.01))
    with tf.variable_scope('hidden2'):
        tf.get_variable(WEIGHTS_STRING, shape=[layer1Size, layer2Size],
                        initializer=tf.random_normal_initializer(0, 0.02))
        tf.get_variable(BIAS_STRING, shape=[layer2Size],
                        initializer=tf.random_uniform_initializer(0, 0.01))
    with tf.variable_scope('output'):
        tf.get_variable(WEIGHTS_STRING,shape=[layer2Size, numLabels],
                        initializer=tf.random_normal_initializer(0, 0.02))
        tf.get_variable(BIAS_STRING, shape=[numLabels],
                        initializer=tf.random_uniform_initializer(0, 0.01))

# ## Defining the Inference Operation
# Here we calculate the output logits (unnormalized scores) for a given input x
# Defining calcutations in the neural network starting from inputs to logits
# logits are the values before applying softmax to the final output
def predictLogit(x):
    # calculations for layer 1
    with tf.variable_scope('hidden1', reuse=True):
        w, b = tf.get_variable(WEIGHTS_STRING), tf.get_variable(BIAS_STRING)
        tf_h1 = tf.nn.relu(tf.matmul(x, w) + b, name = 'h1')
    with tf.variable_scope('hidden2', reuse=True):
        w, b = tf.get_variable(WEIGHTS_STRING), tf.get_variable(BIAS_STRING)
        tf_h2 = tf.nn.relu(tf.matmul(tf_h1, w) + b, name = 'h2')
    # calculations for output layer
    with tf.variable_scope('output', reuse=True):
        w, b = tf.get_variable(WEIGHTS_STRING), tf.get_variable(BIAS_STRING)
        tf_logits = tf.nn.bias_add(tf.matmul(tf_h2, w), b, name = 'logits')
    return tf_logits

def getParam():
    with tf.variable_scope('hidden1', reuse=True):
        wh, bh = tf.get_variable(WEIGHTS_STRING), tf.get_variable(BIAS_STRING)
    with tf.variable_scope('output', reuse=True):
        wo, bo = tf.get_variable(WEIGHTS_STRING), tf.get_variable(BIAS_STRING)
    return wh, bh, wo, bo
    
## Defining Loss Function and the Optimizer
# We use the cross entropy loss function and a momentum-based optimizer for learning
define_net_parameters()
tf_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(logits= predictLogit(tf_inputs), labels=tf_labels))
    # for optimization, we need loss
tf_loss_minimize = tf.train.MomentumOptimizer(momentum=0.9, learning_rate=ρ).minimize(tf_loss)
mnlProbs = tf.nn.softmax(predictLogit(tf_inputs))
    # for the grid we need probabilities, not just loss

## Executing the Graph to get the Classification Results
session = tf.InteractiveSession()
tf.global_variables_initializer().run()

def accuracy(predictions, labels):
    ''' Measure the classification accuracy of some predictions (softmax outputs) 
    and labels (integer class labels)'''
    return np.sum(np.argmax(predictions, axis=1).flatten() == labels.flatten())/batch_size

test_accuracy_over_time = []
train_loss_over_time = []
for epoch in range(NUM_EPOCHS):
    train_loss = []
    ## Training Phase
    for step in range(xtrain.shape[0]//batch_size):
        i = range(step*batch_size, (step+1)*batch_size)
        loss, _ = session.run([tf_loss, tf_loss_minimize],
                              feed_dict={tf_inputs: xtrain[i,:],
                                         tf_labels: labels1hot[i]})
        train_loss.append(loss)
        
    test_accuracy = []
    ## test.  Do it by batch as well as the tensors only take in batch_size inputs
    for step in range(xtest.shape[0]//batch_size):
        i = range(step*batch_size, (step+1)*batch_size)
        testPredictions = session.run(mnlProbs, feed_dict = {tf_inputs: xtest[i,:]})
        batch_test_accuracy = accuracy(testPredictions, ytest[i])
        test_accuracy.append(batch_test_accuracy)
    print("epoch", epoch, " avg train loss:", np.mean(train_loss), "  avg test accuracy:", np.mean(test_accuracy))
    train_loss_over_time.append(np.mean(train_loss))
    test_accuracy_over_time.append(np.mean(test_accuracy)*100)

## Now greate the regular grid
ex1 = np.linspace(x[:,0].min(), x[:,0].max(), 100)
ex2 = np.linspace(x[:,1].min(), x[:,1].max(), 100)
xx1, xx2 = np.meshgrid(ex1, ex2)
                           # unlike R-s 'expand.grid', meshgrid creates matrices
g = np.stack((xx1.ravel(), xx2.ravel()), axis=1)
                           # we create the design matrix by stacking the xx1, xx2
                           # after unraveling those into columns

## predict on the grid
haty = np.empty(shape=(len(g),), dtype='int32')
for step in range(g.shape[0]//batch_size):
    i = range(step*batch_size, (step+1)*batch_size)
    hatp = session.run(mnlProbs, feed_dict = {tf_inputs: g[i,:]})
    haty[i] = np.argmax(hatp, axis=1)
session.close()
hatY = haty.reshape(100, 100)
                           # imshow wants a matrix, so we reshape the predicted
                           # vector into one
plt.imshow(hatY, extent=(x[:,0].min(), x[:,0].max(), x[:,1].min(), x[:,1].max()),
           interpolation='none', origin='lower',
                           # you need to specify that the image begins from below,
                           # not above, otherwise it will be flipped around
           alpha=0.3)
plt.scatter(x[:,0], x[:,1], c=y, edgecolor='k')
plt.show()


## Visualizing the loss and Accuracy
x_axis = np.arange(len(train_loss_over_time))
fig, ax = plt.subplots(nrows=2, ncols=1, figsize=(15,20))
ax[0].semilogy(x_axis, train_loss_over_time)
ax[0].set_xlabel('Epochs',fontsize=18)
ax[0].set_ylabel('Average train loss',fontsize=18)
ax[0].set_title('Training Loss over Time',fontsize=20)
ax[1].plot(x_axis, test_accuracy_over_time)
ax[1].set_xlabel('Epochs',fontsize=18)
ax[1].set_ylabel('Test accuracy',fontsize=18)
ax[1].set_title('Test Accuracy over Time',fontsize=20)
fig.savefig('tfspiral-results.pdf')
