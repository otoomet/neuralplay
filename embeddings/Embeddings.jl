module Embeddings
using SparseArrays
import StatsBase
import Dates
import LinearAlgebra

export gradientAscent
export loglik, gradlik
export buildDTM, positiveContext, tokenize
    
"""
build DT matrix:
It is transposed from what it is normally because of CSC matrix format:
rows: indicators for vocabulary word 
cols: tokens (1-hot encoded)
"""
function buildDTM(tokens, vocabulary)
    dtm = spzeros(Int32, length(vocabulary), length(tokens))
    # try different data types for speed
    for (it, token) in enumerate(tokens)
        j = findfirst(x -> x == token, vocabulary)
        if !(j === nothing)
            dtm[j,it] = 1
        end
    end
    dtm
end


"""
optimizes the function f with gradient ascent
  arguments:
f: loglik function
grad: gradlik function
E₀: initial embeddings (parameters) VxD
WP: positive words (1-hot) NxV
CP: positive context (1-hot) NxV
NP: negative words (1-hot) NxV
NP: negative context (1-hot) NxV
ρ₀: initial learning rate
τ: iteration where learning rate stabilizes
α: momentum parameter (large number: preserve a lot of momentum)
niter: max number of iterations (epochs)
i, j, plot: which two components to plot if 'plot'==true
"""
function gradientAscent(f, grad, E₀, C₀, WP, CP, WN, CN;
                        λ = 0,  # regularization
                        method="adam",
                        ρ₁ = 0.9, ρ₂ = 0.999, δ = 1e-8,  # Adam parameters, see Goodfellow et al p 301
                        ρ₀=0.5, ρₜ=0.005, τ=1_000_000,
                        α,  # momentum parameter
                        batch=nothing,  # SGD batch size (nothing: everything)
                        niter=300, patience=10,
                        verbose=0, verbosestep=10_000)
    ##
    if verbose > 0
        println("gradient ascent using ", method, " algorithm")
    end
    storedLL = Array{Float64}(undef, niter)
    iv = 1
    nP = size(WP, 2)
    nN = size(WN, 2)
    n = nP + nN
    ρ = ρ₀
    if verbose > 0
        println("initial learning rate ", ρ)
    end
    if method == "adam"
        sE = zero(E₀)
        sC = zero(C₀)
        rE = zero(E₀)
        rC = zero(C₀)
        t = 1  # time step
    else
        velocityE = 0
        velocityC = 0
    end
    ## early stopping
    bestE = E₀
    bestC = C₀
    bestGrE = -Inf
    bestGrC = -Inf
    bestLL = f(E₀, C₀, WP, CP, WN, CN, 1:nP, 1:nN; λ=λ)
    if verbose > 0
        println("initial log-likelihood:", bestLL)
    end
    iWorse = 0
    ## ----- batch indices
    ## size of the batch
    negSize = size(WN, 2) ÷ size(WP,2)
    pBatchSize = batch == nothing ? size(WP, 2) : batch
    nBatchSize = batch == nothing ? size(WN, 2) : batch*negSize
    ## number of batches (the same for positive/negative)
    nBatch = size(WP, 2) ÷ pBatchSize  # total number of batches
    if verbose > 0
        println(nBatch, " batches")
    end
    ## shuffled indices
    iPSeq = StatsBase.randperm(size(WP, 2))
    iNSeq = StatsBase.randperm(size(WN, 2))
    ##
    startTime = Dates.now()
    iteration = niter  # record where it lost patience
    for epoch in 1:niter
        grE = nothing  # define global epoch variables
        grC = nothing
        for ib = 1:nBatch
            ## indices for this batch
            iP = iPSeq[ib:nBatch:nP]
            iN = iNSeq[ib:nBatch:nN]
            ##
            grE, grC = grad(E₀, C₀, WP, CP, WN, CN, iP, iN; λ=λ)
            if method == "adam"
                sE = ρ₁ * sE + (1 - ρ₁) * grE
                sC = ρ₁ * sC + (1 - ρ₁) * grC
                rE = ρ₁ * rE + (1 - ρ₂) * grE.*grE
                rC = ρ₁ * rC + (1 - ρ₂) * grC.*grC
                sEHat = sE / (1 - ρ₁^t)
                sCHat = sC / (1 - ρ₁^t)
                rEHat = rE / (1 - ρ₂^t)
                rCHat = rC / (1 - ρ₂^t)
                E = E₀ + ρ * sEHat ./ (sqrt.(rEHat) .+ δ)
                C = C₀ + ρ * sCHat ./ (sqrt.(rCHat) .+ δ)
                t += 1
            else
                velocityE = α*velocityE .+ (1-α)*grE
                velocityC = α*velocityC .+ (1-α)*grC
                E = E₀ + ρ * velocityE;
                C = C₀ + ρ * velocityC
            end
            E₀ = E
            C₀ = C
        end  # loop over batches
        ## printing: only at the end of epoch
        if epoch % verbosestep == 0
            ## predict on validation set
            ll = f(E₀, C₀, WP, CP, WN, CN, 1:nP, 1:nN; λ=λ)
            storedLL[iv] = ll
            iv += 1
            if method != "adam"
                ρ = max(ρ₀ - epoch/τ*(ρ₀ - ρₜ), ρₜ)
            end
            # linear decay of learning rate, see GBC p287
            if ll > bestLL
                bestE = E₀
                bestC = C₀
                bestGrE = grE
                bestGrC = grC
                bestLL = ll
                iWorse = 0
            else
                if iWorse >= patience
                    if verbose > 0
                        println("lost patience ", patience)
                    end
                    iteration = epoch
                    break
                end
                iWorse += 1
            end
            if verbose > 0
                endTime = Dates.now()
                println("epoch ", epoch,
                        ": test ll =", ll, ", ρ=", ρ,
                        "  elapsed ", (Dates.value(endTime) - Dates.value(startTime))/1000, " seconds")
                startTime = Dates.now()
                if verbose > 1
                    H = Hess(E, C, λ=λ, ε=1e-6)
                    println("Condition number of Hessian ", cond(H))
                end
            end
        end 
        iteration = epoch
    end
    if verbose > 0
        println("$iteration iterations done")
        println("E gradient norm ", LinearAlgebra.norm(bestGrE),
                ", C gradient norm ", LinearAlgebra.norm(bestGrC))
    end
    (bestE, bestC, bestGrE, bestGrC, bestLL, iteration, storedLL)
end

"""
E: embeddings matrix
"""
function gradlik(E, C, WP, CP, WN, CN, iP, iN; λ=0)
    ## E: word embeddings
    ## C: context embeddings
    ## iP, iN: batch indices for positive, negative samples
    ## log likelihood (only positive examples)
    gradE = zero(E)
    gradC = zero(C)
    ## positive context
    W = E * WP[:,iP]
    K = C * CP[:,iP]
    similarity = sum(W .* K, dims=1)
    p = 1 ./ (1 .+ exp.(similarity))  # Nx1
    gradE = WP[:,iP] * (p .* K)'
    gradC = CP[:,iP] * (p .* W)'
    ## negative context
    W = E * WN[:,iN]
    K = C * CN[:,iN]
    similarity = sum(W .* K, dims=1)
    p = -1 ./ (1 .+ exp.(-similarity))  # Nx1
    gradE += WN[:,iN] * (p .* K)'
    gradC += CN[:,iN] * (p .* W)'
    #
    n = length(iP) + length(iN)
    gradE'/n - 2*λ*E, gradC'/n - 2*λ*C
end

"""
E: embeddings matrix
"""
function loglik(E, C, WP, CP, WN, CN, iP, iN; λ=0)
    ## E: word embeddings
    ## C: context embeddings
    ## iP, iN: batch indices for positive, negative samples
    ## log likelihood (only positive examples)
    similarity = sum((E * WP[:,iP]) .* (C * CP[:,iP]), dims=1)
    l = -sum(log.(1 .+ exp.(-similarity)))
    ## negative examples
    similarity = sum((E * WN[:,iN]) .* (C * CN[:,iN]), dims=1)
    l -= sum(log.(1 .+ exp.(similarity)))
    # Expected value for negative likelihood (see Li et al, 2019)
    n = length(iP) + length(iN)
    l/n - λ*(sum(E.*E) + sum(C.*C))
end

"""
Create positive context matrix
Will be of the same type as DTM, normally sparse CSC
"""
function positiveContext(tokens, vocabulary, DTM, window)
    CP = similar(DTM, length(vocabulary), length(tokens) - 2*window);
    # -2*window: we ignore the incomplete context in beginning, end
    iWindow = vcat(collect(-window:-1), collect(1:window))
    DTMd = Matrix(DTM)  # make it dense to speed up the process
    for it in window+1:length(tokens)-window
        # it: token
        ic = it - window;
        # ic: context
        CP[:,ic] = sum(DTMd[:, it .+ iWindow], dims=2)
    end
    CP
end

function tokenize(text)
    stopwords = ["", "the", "of", "to", "in", "that", "and", "is", "a", "from", "for", "with",
                 "or", "were", "same", "other", "by", "what", "may", "but", "those", "have", "this",
                 "could", "are", "be", "you", "we", "as", "i", "it", "our", "will", "your", "not", "which",
                 "its", "all", "at", "so", "on", "shall", "an", "such", "their", "they", "only", "has", "do",
                 "very", "than", "into", "upon", "me", "my", "some", "one", "his", "who", "there", "these",
                 "was", "more", "us", "through", "her", "no", "them", "been", "when", "many", "yes", "would",
                 "over"]
    # stopwords, include empty strings here too
    txt = lowercase(text)
    txt = replace(txt, r"'" => "")  # can't -> cant
    txt = replace(txt, r"[][\r\n().,;/:‐—_”‘“#$?*™’§&%><-]" => " ")
    txt = replace(txt, r"""["]""" => " ")
    # apparently ess cannot handle quotes in quotes
    txt = replace(txt, r"[.!]( |$)" => " ")
    txt = replace(txt, r"[[:digit:]]" => " ")
    txt = replace(txt, r" +" => " ")
    tokens = split(txt, " ")
    tokens[tokens .∉ [stopwords]]
end

end # module
