#!/usr/bin/env python3
import tensorflow as tf
print("tensorflow version:", tf.__version__)
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer
from nltk.util import ngrams
import nltk
import re

with open('embeddingtext.txt', 'r') as infile:
   text = infile.read().replace('\n', ' ').lower()
text = re.sub(r'[^a-zA-Z0-9\s]', ' ', text)
tokens = nltk.word_tokenize(text)
bigrams = [bg for bg in nltk.bigrams(tokens)]
## -------------------- create ordered counts
print("  create ordered counts")
counts = { bg[0] : {} for bg in bigrams}
for bg in bigrams:
   w0 = bg[0]
   w1 = bg[1]
   try:
      counts[w0][w1] += 1
   except:
      counts[w0][w1] = 1
for k in counts.keys():
   print(k, counts[k])

bigrams = [' '.join(bg) for bg in bigrams]
print(bigrams)

vectorizer = CountVectorizer(bigrams)
bow = vectorizer.fit_transform(bigrams).todense()
bow = pd.DataFrame(bow, columns=vectorizer.get_feature_names())
print("shape:", bow.shape)
print(bow)
exit(0)


x, y = createSpiral(1000)
xtrain, xtest, ytrain, ytest = train_test_split(x, y, test_size=0.2)


model = tf.keras.models.Sequential([
   tf.keras.layers.Input(shape=(2,)),
   tf.keras.layers.Dense(50, activation='relu'),
   tf.keras.layers.Dense(4, activation='softmax')
])

model.compile(optimizer=tf.keras.optimizers.SGD(learning_rate=0.1),
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

model.fit(xtrain, ytrain, epochs=300, batch_size=10, verbose=1)
print("fitting done")
p = model.evaluate(xtest, ytest, verbose=0)
print("final peformance:\n",
      np.stack((model.metrics_names, p), axis=1))

## Now greate the regular grid
ex1 = np.linspace(x[:,0].min(), x[:,0].max(), 100)
ex2 = np.linspace(x[:,1].min(), x[:,1].max(), 100)
xx1, xx2 = np.meshgrid(ex1, ex2)
                           # unlike R-s 'expand.grid', meshgrid creates matrices
g = np.stack((xx1.ravel(), xx2.ravel()), axis=1)
                           # we create the design matrix by stacking the xx1, xx2
                           # after unraveling those into columns
## predict on the grid
phat = model.predict(g)
hatY = np.apply_along_axis(np.argmax, 1, phat).reshape(100,100)
                           # imshow wants a matrix, so we reshape the predicted
                           # vector into one
plt.imshow(hatY, extent=(x[:,0].min(), x[:,0].max(), x[:,1].min(), x[:,1].max()),
           interpolation='none', origin='lower',
                           # you need to specify that the image begins from below,
                           # not above, otherwise it will be flipped around
           alpha=0.3)
plt.scatter(x[:,0], x[:,1], c=y, edgecolor='k')
plt.show()
