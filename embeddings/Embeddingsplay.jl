module Embeddingsplay

export closestWords, similarity

import DataFrames
import LinearAlgebra
import CSV

"""
E: word embeddings
C: context embeddings
"""
function analyze(E, C)
    ## positive probabilities
    words = ["wa", "wb"];
    w = WP[tokens[2:end-1] .∈ [words], :];
    c = CP[tokens[2:end-1] .∈ [words], :];
    v = w * E;
    k = c * C;
    p = 1 ./ (1 .+ exp.(sum(v .* k, dims=2)));
    Pdf = convert(DataFrames.DataFrame, p);
    DataFrames.insertcols!(Pdf, 1, :token => tokens[tokens .∈ [words]]);
    println("\npositive probabilities")
    display(Pdf)
    ## negative
    trueTokens = repeat(tokens[2:end-1], inner=2);
    nTokens = CNdf.negToken
    wn = WN[nTokens .∈ [words], :];
    cn = CN[nTokens .∈ [words], :];
    vn = wn * E;
    kn = cn * C;
    pn = 1 ./ (1 .+ exp.(sum(vn .* kn, dims=2)));
    PNdf = convert(DataFrames.DataFrame, pn);
    DataFrames.insertcols!(PNdf, 1, :negToken => nTokens[nTokens .∈ [words]]);
    DataFrames.insertcols!(PNdf, 1, :posToken => trueTokens[nTokens .∈ [words]]);
    println("\nnegative probabilities")
    display(PNdf)
end

"""
Print the most common closest words according to embedding matrix E
"""
function closestWords(E::Array, 
                      orderedTokens,
                      vocabulary;
                      nStore=5,
                      verbose=0)
    products = E'*E;
    norms = sqrt.(LinearAlgebra.diag(products));
    similarities = products ./ norms ./norms'
    i = 1:min(8, size(similarities, 1))
    if verbose > 0
        println("first 8 similarities:")
        display(similarities[i,i]); println()
    end
    words = [ [] for i in 1:size(E, 1) ]
    for i in 1:size(E, 1)
        w = orderedTokens[i][1]
        iv = findfirst(x -> x == w, vocabulary)
        j = sortperm(similarities[iv,:], rev=true)
        words[i] = [orderedTokens[i][1], vocabulary[j[2:min(nStore + 1, length(vocabulary))]] ]
    end
    words
end    

"""
find the closes words to "word"
"""
function closestWords(word::String, df::DataFrames.DataFrame, n=10)
    if word ∉ df.token
        println("no such word")
        return
    end
    i = findall(x -> x == word, df.token)
    E = convert(Array, CSV.select(df, CSV.Not(:token)))
    products = E[i,:]*E';
    norms = sqrt.(sum(E .* E, dims=2));
    similarities = products ./ norms[i] ./norms'
    j = sortperm(vec(similarities), rev=true)
    [(df.token[i], round(similarities[i], digits=3)) for i in j[2:n]]
end    

"""
find the closes words to worda + wordb - wordc
for instance: woman + king - man
i.e. "man is to king what woman is to X"
"""
function closestWords(worda::String, wordb::String, wordc::String,
                      df::DataFrames.DataFrame, n=10)
    if !([worda, wordb, wordc] ⊆ df.token)
        println("no such words")
        return
    end
    ia = findall(x -> x == worda, df.token)[1]
    ib = findall(x -> x == wordb, df.token)[1]
    ic = findall(x -> x == wordc, df.token)[1]
    E = convert(Array, CSV.select(df, CSV.Not(:token)))
    v = (E[ic,:] - E[ia,:] + E[ib,:])'
    df3 = convert(DataFrames.DataFrame, vcat(E[[ia,ib,ic],:], v))
    CSV.insertcols!(df3, 1, :word => [worda, wordb, wordc, "c-a+b"])
    display(df3)
    products = v*E';
    norms = sqrt.(sum(E .* E, dims=2));
    similarities = products ./ LinearAlgebra.norm(v) ./norms'
    j = sortperm(vec(similarities), rev=true)
    [(df.token[i], round(similarities[i], digits=3)) for i in j[1:n]]
end

"""
compute cosine similarity b/w worda, wordb
"""
function similarity(worda::String, wordb::String, df::DataFrames.DataFrame)
    if !([worda, wordb] ⊆ df.token)
        println("no such words")
        return
    end
    ia = findall(x -> x == worda, df.token)[1]
    ib = findall(x -> x == wordb, df.token)[1]
    E = convert(Array, CSV.select(df[[ia, ib],:], CSV.Not(:token)))
    va = E[1,:]
    vb = E[2,:]
    s = va' * vb/LinearAlgebra.norm(va)/LinearAlgebra.norm(vb)
    s
end

end # module
