#!/usr/bin/env julia

using Random;
using Distributions;
using LinearAlgebra;
import StatsBase;                  
using ArgParse;
import CSV;
import DataFrames;
import CodecBzip2
import Dates
using SparseArrays

push!(LOAD_PATH, expanduser("~/tyyq/social/evictions/neuralplay/embeddings"))
using Embeddings
using Embeddingsplay

function numgrad(E, C, WP, CP, WN, CN, iP, iN; λ=0, ε=1e-6)
    l₀ = loglik(E, C, WP, CP, WN, CN, iP, iN; λ=λ)
    l₁ = Array{Float64}(undef, size(E,1) + size(C,1), size(E,2))
    EC = vcat(E, C)
    for i in 1:size(EC, 1)
        for j in 1:size(EC, 2)
            EE = deepcopy(EC)
            EE[i,j] += ε
            l₁[i,j] = loglik(EE[1:size(E,1),:], EE[size(E,1)+1:end,:], WP, CP, WN, CN, iP, iN; λ=λ)
        end
    end
    (l₁ .- l₀)/ε
end    

function comparederivatives(λ=0, ε=1e-6, batch=nothing)
    if batch == nothing
        iP = 1:size(WP,1)
        iN = 1:size(WN,1)
    end
    num = numgrad(E, C, WP, CP, WN, CN, iP, iN; λ=λ, ε=ε)
    print("dim of numeric:", size(num), "\n")
    display(num)
    anal = gradlik(E, C, WP, CP, WN, CN, iP, iN; λ=λ)
    anal = vcat(anal[1], anal[2])
    print("dim of analytic:", size(anal), "\n")
    display(anal)
    println("relative difference")
    rel = (num-anal)./anal
    display(rel)
    rel0 = [isnan(x) ? 0 : x for x in rel]
    println("max difference ", maximum(abs.(rel0)), " at ", findmax(abs.(rel0))[2])
    return num, anal
end

function Hess(E, C; λ=0, ε = 1e-6)
    θ₀ = vcat(vec(E), vec(C))  # full parameter vector
    H = Array{Float64}(undef, length(θ₀), length(θ₀))
    grE, grC = gradlik(E, C, WP, CP, WN, CN; λ=λ)
    v₀ = vcat(vec(grE), vec(grC))
    for i in 1:length(θ₀)
        θ = copy(θ₀)
        θ[i] += ε
        EE = reshape(θ[1:Integer(length(θ)/2)], size(E, 1), size(E, 2))
        CC = reshape(θ[Integer(length(θ)/2)+1:end], size(C, 1), size(C, 2))
        grE, grC = gradlik(EE, CC, WP, CP, WN, CN; λ=λ)
        v = vcat(vec(grE), vec(grC))
        H[:,i] = (v - v₀)/ε
    end
    H
end

s = ArgParseSettings()
@add_arg_table! s begin
    "--batch", "-b"
    help = "batch size (nothing: full batch)"
    arg_type = Int
    default = nothing
    "--file", "-f"
    help = "input file name"
    arg_type = String
    default = "../texts/embeddingtext.txt"
    "--dimension", "-D"
    help = "embeddings dimension"
    arg_type = Int
    default = 20
    "--negSize"
    help = "negative sampling size (--negSize)"
    arg_type = Int
    default = 2
    "--lambda", "-l"
    help = "log-likelihood L2 penalty"
    arg_type = Float64
    default = 0.0
    "--method"
    help = "optimization method"
    arg_type = String
    default = "adam"
    "--window", "-w"
    help = "context window half-width"
    arg_type = Int
    default = 3
    "--minFreq"
    help = "minimum token frequency"
    arg_type = Int
    default = 3
    "--niter", "-i"
    help = "max number of iterations"
    arg_type = Int
    default = 1000
    "--delta", "-d"
    help = "Adam's stability δ"
    arg_type = Float64
    default = 1e-8
    "--rho", "-r"
    help = "Initial learning rate"
    arg_type = Float64
    default = 0.1
    "--rt" 
    help = "final learning rate at iteration τ"
    arg_type = Float64
    default = 0.01
    "--alpha"
    help = "momentum parameter α"
    arg_type = Float64
    default = 0.9
    "--seed", "-s"
    help = "random seed"
    arg_type = Int
    default = 0
    "--tau", "-t"
    help = "Iteration where rho set to max rho"
    arg_type = Int
    default = 10000
    "--verbose", "-v"
    help = "verbosity level"
    arg_type = Int
    default = 1
    "--vs"
    help = "verbosity printing step"
    arg_type = Int
    default = 50
end

args = parse_args(ARGS, s)
println("  input file: ", args["file"])
D = args["dimension"];  # embedding dimension
println("  embeddings dimension: ", D)
window = args["window"];  
println("  context window half-width (--window, -w): ", window)
minFreq = args["minFreq"];  
println("  minimum token frequency: ", minFreq)
negSize = args["negSize"];
println("  negative sampling size (--negSize): ", negSize)
λ = args["lambda"]
println("  penalty λ (--lambda, -l):", λ)
nIter = args["niter"]
println("  n iterations: ", nIter)
batch = args["batch"]
if batch === nothing
    println("  full gradient ascent (--batch)")
else
    println("  stochastic gradient ascent, batch size (--batch): ", args["batch"])
    println("  batch size: ", args["batch"])
end
method = args["method"]
println("  ", method, " optimizer (--method)")
ρ = args["rho"]
println("  learning rate (--rho): ", ρ)
δ = args["delta"]
τ = args["tau"]
ρₜ = args["rt"]
α = args["alpha"]
println("  α (--alpha):", α)
if method == "adam"
    println("  Adam's stability δ (--delta): ", δ)
else
    println("  τ (--tau, -t):", τ)
    println("  ρₜ (--rt):", ρₜ)
end
seed = args["seed"]
println("  random seed (--seed): ", seed)
verbose = args["verbose"]
println("  verbosity=", verbose)
vs = args["vs"]
println("  verbosity printing step (--vs): ", vs)
fName = args["file"]
  
"""
mold data preparation into a function for repl
window: half-width of the context window (2 means 2 words before/after)
"""
function prepare(fName, minFreq=3, window=2, verbose=1)
    println("minimum token frequency minFreq: ", minFreq)
    rng = MersenneTwister(1)
    ## embeddings
    ## read and tokenize file
    global text
    if endswith(fName, ".bz2")
        compressed = read(fName);
        plain = transcode(CodecBzip2.Bzip2Decompressor, compressed);
        text = String(plain);
    else
        text = open(f -> read(f, String), fName);
    end
    global tokens, tokenCounts
    tokens = tokenize(text);
    tokens = append!(["BEG"], tokens)
    tokens = append!(tokens, ["END"])
    println(length(tokens), " tokens")
    global tokenCounts
    tokenCounts = StatsBase.countmap(tokens);
    global vocabulary
    vocabulary = sort(collect(keys(tokenCounts)))
    # vocabulary is alphabetically sorted
    ## only frequent tokens
    tokens = [t for t in tokens if tokenCounts[t] >= minFreq]
    vocabulary = [v for v in vocabulary if tokenCounts[v] >= minFreq]
    println("vocabulary size ", length(vocabulary), " (only words at least ", minFreq, " times present)")
    println("First vocabulary words:", vocabulary[1:min(10, length(vocabulary))])
    println("Last vocabulary words:", vocabulary[max(end-10,1):end])
    println("Sample tokens:", sample(vocabulary, min(10, length(vocabulary)), replace=false))
    global orderedTokens, V
    orderedTokens = sort(collect(tokenCounts), by=e->e[2], rev=true)
    # tokens ordered by frequecy (essentially vocabulary)
    # we only need that for testing and such
    if verbose > 0
        println("most frequent tokens:")
        println(orderedTokens[1:min(6, length(orderedTokens))])                  
        println("\nleast frequent tokens:")
        println(orderedTokens[max(1, end-6):end])                  
        println("\nsample of tokens:")
        println(sample(orderedTokens, min(6, length(orderedTokens)), replace=false)); println()
    end
    ## set up params
    N = length(tokens);  # words
    V = length(vocabulary)  # vocabulary size
    ## Build dtm and contexts
    global DTM
    DTM = buildDTM(tokens, vocabulary);
    nTokens = size(DTM, 2)
    println(size(DTM, 1), "x", nTokens, " DTM of type ", typeof(DTM))
    global WP
    WP = DTM[:,window+1:end-window]
    if verbose > 0
        println(size(WP, 1), "x", size(WP, 2), " positive words, type", typeof(WP))
    end
    ## positive context
    global CP
    startTime = Dates.now()
    CP = positiveContext(tokens, vocabulary, DTM, window)
    if verbose > 0
        println("  elapsed ", (Dates.value(Dates.now()) - Dates.value(startTime))/1000, " seconds")
        println(size(CP, 1), "x", size(CP, 2), " positive context, type ", typeof(CP))
    end
    ## negative context
    iNeg = repeat(1:size(CP,2), inner=negSize)
    global WN, CN
    CN = CP[:,iNeg];  # negative context
    println(size(CN, 1), "x", size(CN, 2), " negative context, type ", typeof(CN))
    WN = similar(WP, size(CN,1), size(CN, 2));
    # negative words
    println("doing negative words")
    for iToken in window+1:nTokens-window
        ic = iToken - window  # context: -window because we ignore incomplete context in beginning
        validTokens = filter(x -> x ≠ tokens[iToken], vocabulary)
        validCounts = [tokenCounts[v]^0.75 for v in validTokens]
        negSample = sample(validTokens, StatsBase.Weights(validCounts), negSize);
        # sampled wrong words
        j = 1 + negSize*(ic - 1) : negSize*ic;  # position in the negative words array
        # positions where to put these words
        is = [findfirst(x -> x == t, vocabulary) for t in negSample];
        # positions of wrong words in the vocabulary
        for jis in zip(j, is)
            jj, iis = jis
            WN[iis,jj] = 1;
        end
    end
    if verbose > 0
        println(size(WN, 1), "x", size(WN, 2), " negative words, type ", typeof(WN))
        println(size(CN, 1), "x", size(CN, 2), " negative context, type ", typeof(CN))
    end
    ## initialize
    global E, C
    E = rand(Uniform(-1/2/D, 1/2/D), D, V);
    C = rand(Uniform(-1/2/D, 1/2/D), D, V);
    println("word embeddings ", size(E, 1), "x", size(E, 2), ", mean abs value ", mean(abs.(E)))
    println("context embeddings ", size(C, 1), "x", size(C, 2), ", mean abs value ", mean(abs.(C)))
    println("  in all ", length(E) + length(C), " dimensional problem")
end

prepare(fName, minFreq, window)

function printClosestWords(words, nPrint=19)
    for i in 1:min(size(words, 1), nPrint)
        println(" ", words[i][1], " ", join(words[i][2:end], " "))
    end
end

"""
    Encapsulate the embeddings computation and all that into a function for repl
    """
function embeddings(E, C, WP, CP, WN, CN, nPrint=15)
    println("compute embeddings and save in a csv file")
    global e, c, grE, grC, val, n
    e, c, grE, grC, val, n, ll = gradientAscent(loglik, gradlik, E, C, WP, CP, WN, CN,
                                                niter=nIter, batch=batch,
                                                λ=λ,
                                                method=method,
                                                δ = δ,  # adam coefs
                                                ρ₀ = ρ, τ = τ, ρₜ = ρₜ,
                                                α=α,
                                                verbose=1, verbosestep=vs);
    global Edf, Cdf
    Edf = convert(DataFrames.DataFrame, e');
    println("created data frame of size ", size(Edf))
    println("vocabulary size ", size(vocabulary))
    DataFrames.insertcols!(Edf, 1, :token => vocabulary)
    Cdf = convert(DataFrames.DataFrame, c');
    DataFrames.insertcols!(Cdf, 1, :token => vocabulary)
    CSV.write("embeddings.csv", Edf)
    w = closestWords(e, orderedTokens, vocabulary)
    printClosestWords(w, nPrint)
end

"""
grid search for best ρ, batch size
"""
function searchρb(verbose=0)
    rhos = [0.0001 0.0002 0.0005]
    batches = [ 10, 50, 233, 1000]
    results = Dict()
    for ρ in rhos
        for batch in batches
            print(ρ, " ", batch, " ")
            startTime = Dates.now()
            e, c, grE, grC, val, n, ll = gradientAscent(loglik, gradlik, E, C, WP, CP, WN, CN,
                                                        niter=nIter, batch=batch,
                                                        λ=λ,
                                                        method=method,
                                                        δ = δ,  # adam coefs
                                                        ρ₀ = ρ, τ = τ, ρₜ = ρₜ,
                                                        α=α,
                                                        verbose=verbose, verbosestep=vs);
            results[(ρ, batch)] = val
            endTime = Dates.now()
            println(val, "  elapsed ", (Dates.value(endTime) - Dates.value(startTime))/1000, " seconds")
        end
    end
    df = DataFrames.DataFrame(collect(keys(results)))
    DataFrames.rename!(df, [:ρ, :batch])
    DataFrames.insertcols!(df, 3, :ll => collect(values(results)))
    sort!(df)
    CSV.write("rho-batch.csv", df)
    df
end

"""
runs the estimates many times and returns the array of likelihoods
"""
function sampleLL(n)
    println("Embedding dimension D: ", D)
    println("regularization λ: ", λ)
    lls = Array{Float64}(undef, n)
    words = Array{String}(undef, n)
    for i in 1:n
        println(" – iteration ", i, " – ")
        E = rand(Uniform(-1/2/D, 1/2/D), V, D);
        C = rand(Uniform(-1/2/D, 1/2/D), V, D);
        e, c, grE, grC, val, n = gradientAscent(loglik, gradlik, E, C, WP, CP, WN, CN,
                                                niter=nIter,
                                                λ=λ,
                                                method="adam",
                                                δ = δ,  # adam coefs
                                                ρ₀ = ρ, τ = τ, ρₜ = ρₜ,
                                                α=α,
                                                verbose=1, verbosestep=vs);
        lls[i] = val
        w = closestWords(e)
        words[i] = w[1][2][1]
        printClosestWords(w, 5)
    end
    lls, words
end

## Potential finals

# embeddings(E, C, WP, CP, WN, CN, 20)
searchρb()
