#!/usr/bin/env python

import numpy as np 

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def sigmoid_derivative(x):
    return x * (1 - x)

training = np.array([[0,0,1],
                     [1,1,1],
                     [1,0,1],
                     [0,1,1]])

t_output = np.array([[0,1,1,0]]).T 


np.random.seed(1)

weights = 2 * np.random.random((3,1)) - 1
weights

for interation in range(1000):

    input_layer = training

    outputs = sigmoid(np.dot(input_layer, weights))

    error = t_output - outputs

    adjustments = error * sigmoid_derivative(outputs)

    weights += np.dot(input_layer.T, adjustments)

weights 

outputs

